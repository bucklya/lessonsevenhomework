package com.company;

import com.company.taskone.data.Generator;
import com.company.taskone.model.Bank;
import com.company.taskone.model.BlackMarket;
import com.company.taskone.model.CurrencyOrganization;
import com.company.taskone.model.Exchanger;

import java.util.Scanner;

public class TaskOne {

    public static void runTaskOne() {
        CurrencyOrganization[] organizations = Generator.generate();
        Scanner scan = new Scanner(System.in);
        System.out.println("Какое сумму денег вы хотите сконвентировать: ");
        double inputMoney = Double.parseDouble(scan.nextLine());

        for (CurrencyOrganization organization : organizations) {
            if (organization instanceof Bank) {
                Bank bank = (Bank) organization;
                if (bank.checkIPALimit(inputMoney)) {
                    bank.convert(inputMoney);
                }
            }

            if (organization instanceof Exchanger) {
                Exchanger exchanger = (Exchanger) organization;
                if (exchanger.CheckUSDLimit(inputMoney)) {
                    exchanger.convert(inputMoney);
                }
            }

            if (organization instanceof BlackMarket) {
                BlackMarket blackMarket = (BlackMarket) organization;
                organization.convert(inputMoney);
            }
        }

    }
}
