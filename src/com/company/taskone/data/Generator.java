package com.company.taskone.data;

import com.company.taskone.model.Bank;
import com.company.taskone.model.BlackMarket;
import com.company.taskone.model.CurrencyOrganization;
import com.company.taskone.model.Exchanger;

public final class Generator {
    private Generator() {
    }

    public static CurrencyOrganization[] generate() {
        CurrencyOrganization[] organizations = {
                new Bank("ПриватБанк", 26.65d),
                new Bank("ОщадБанк", 26.75d),
                new Bank("УкрСибБанк", 26.7d),
                new Exchanger("КитGroup", 26.6d),
                new Exchanger("Money24/7", 26.9d),
                new BlackMarket("Black Market", 26.95d)
        };
        return organizations;
    }
}
