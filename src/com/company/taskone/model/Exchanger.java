package com.company.taskone.model;

public class Exchanger extends CurrencyOrganization{
    private double usdLimit = 20000;

    public Exchanger(String name, double usd) {
        super(name, usd);
    }

    public boolean CheckUSDLimit(double money) {
        double result = money*getUsd();
        if (result < usdLimit) {
            return true;
        } else {
            System.out.println(String.format("Операция в обменнике %s невозможна!!!", super.getName()));
            return false;
        }
    }
}
