package com.company.taskone.model;

public class CurrencyOrganization {
    private String name;
    private double usd;

    public CurrencyOrganization(String name, double usd) {
        this.name = name;
        this.usd = usd;
    }

    public void convert(double money){
        System.out.println(String.format("Ваша сумма по курсу %.2f организации: %s в USD будет %.2f ", usd, name, money*usd));
    }

    public String getName() {
        return name;
    }

    public double getUsd() {
        return usd;
    }
}
