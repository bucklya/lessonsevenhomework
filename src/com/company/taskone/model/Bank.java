package com.company.taskone.model;

public class Bank extends CurrencyOrganization {
    private double ipaLimit = 150000;

    public Bank(String name, double usd) {
        super(name, usd);
    }

    public double getIpaLimit() {
        return ipaLimit;
    }

    public boolean checkIPALimit(double money) {
        if (money < ipaLimit) {
            return true;
        } else {
            System.out.println(String.format("Операция в банке %s невозможна!!!", super.getName()));
            return false;
        }

    }
}
