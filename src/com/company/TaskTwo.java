package com.company;

import com.company.tasktwo.data.Generator;
import com.company.tasktwo.model.Book;
import com.company.tasktwo.model.Literature;
import com.company.tasktwo.model.Magazine;
import com.company.tasktwo.model.Yearbook;

import java.util.Scanner;

public class TaskTwo {
    public static void runTaskTwo() {
        Literature[] literature = Generator.generate();
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите год для вывода всей литературы которая была издана в этом году: ");
        int inputYear = Integer.parseInt(scan.nextLine());

        for (Literature literatures : literature) {
            if (literatures.isThatYear(inputYear)) {
                System.out.println(literatures);
            }
        }

        System.out.println("Только книги: ");
        for (Literature literatures : literature) {
            if (literatures instanceof Book){
                System.out.println(literatures);
            }
        }

        System.out.println("Только журналы: ");
        for (Literature literatures : literature) {
            if (literatures instanceof Magazine){
                System.out.println(literatures);
            }
        }

        System.out.println("Только ежегодники: ");
        for (Literature literatures : literature) {
            if (literatures instanceof Yearbook){
                System.out.println(literatures);
            }
        }
    }
}
