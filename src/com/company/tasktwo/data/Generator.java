package com.company.tasktwo.data;

import com.company.tasktwo.model.Book;
import com.company.tasktwo.model.Literature;
import com.company.tasktwo.model.Magazine;
import com.company.tasktwo.model.Yearbook;

public final class Generator {
    private Generator() {
    }

    public static Literature[] generate() {
        Literature[] literature = {
                new Book("Три мушкетера", 2014, "А. Дюма", "КиевПечать"),
                new Book("Самые голубые глаза", 2020, "Т. Моррисон", "Эксмо"),
                new Book("Атлант расправил плечи", 2012, "А. Рэнд", "Альпина Диджитал"),
                new Magazine("Time", 2020, "Новости", "Февраль"),
                new Magazine("Автолегенды СССР", 2019, "Автомобили", "Октябрь"),
                new Magazine("Музика", 2019, "Музыка", "Декабрь"),
                new Yearbook("Япония", 2020, "Наука", "Ассоциация японоведов"),
                new Yearbook("Харьковский историографический сборник", 2019, "История", "исторический факультет ХНУ имени В. Н. Каразина.")
        };
        return literature;
    }
}
