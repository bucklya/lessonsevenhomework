package com.company.tasktwo.model;

public class Magazine extends Literature {
    private String subjects;
    private String monthPublication;

    public Magazine(String title, int yearPublication, String subjects, String monthPublication) {
        super(title, yearPublication);
        this.subjects = subjects;
        this.monthPublication = monthPublication;
    }

    @Override
    public String toString() {
        return String.format("%s | Тематика: %s | Месяц издания: %s", super.toString(), subjects, monthPublication);
    }
}
