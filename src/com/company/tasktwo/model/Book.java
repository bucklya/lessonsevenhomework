package com.company.tasktwo.model;

public class Book extends Literature {
    private String author;
    private String publishingHouse;

    public Book(String title, int yearPublication, String author, String publishingHouse) {
        super(title, yearPublication);
        this.author = author;
        this.publishingHouse = publishingHouse;
    }

    @Override
    public String toString() {
        return String.format("%s | Автор: %s | Издательство: %s", super.toString(), author, publishingHouse);
    }
}
