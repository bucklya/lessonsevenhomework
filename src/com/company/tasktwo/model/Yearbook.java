package com.company.tasktwo.model;

public class Yearbook extends Literature {
    private String subjects;
    private String publishingHouse;

    public Yearbook(String title, int yearPublication, String subjects, String publishingHouse) {
        super(title, yearPublication);
        this.subjects = subjects;
        this.publishingHouse = publishingHouse;
    }

    @Override
    public String toString() {
        return String.format("%s | Тематика: %s | Издательство: %s", super.toString(), subjects, publishingHouse);
    }
}
