package com.company.tasktwo.model;

public class Literature {
    private String title;
    private int yearPublication;

    public Literature(String title, int yearPublication) {
        this.title = title;
        this.yearPublication = yearPublication;
    }

    @Override
    public String toString() {
        return String.format("Название %s | Год публикации %d", title, yearPublication);
    }

    public Boolean isThatYear(int year) {
        return year == yearPublication;
    }
}
